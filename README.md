# CLI Log Tool

### Compile

```bash
cargo build --release
```

### Usage

```bash
./target/release/ftypec --help
```

### Example

```bash
./data/gen_data.sh 10 | ./target/release/ftypec
```