use std::path::PathBuf;

use structopt::StructOpt;

use crate::parser::Source;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "file log tool",
    about = "Cli tool that outputs JSON object value (type key) frequency and the total message size."
)]
pub struct CliOptions {
    #[structopt(
        short,
        long,
        help = "Display parsing warnings e.g. Invalid lines; Invalid JSON objects; JSON without keys and; empty key values"
    )]
    warnings: bool,

    #[structopt(
        parse(from_os_str),
        help = "If not present stdin will be used as a data source"
    )]
    file: Option<PathBuf>,

    #[structopt(short, long, help = "Use SI unit suffixes in the size column")]
    human_readable: bool,

    #[structopt(short)]
    number_of_threads: Option<usize>,

    #[structopt(short, long, help = "Output table sorted by column number [1-3]")]
    sort_column_number: Option<u8>,
}

impl CliOptions {
    pub fn warnings(&self) -> bool {
        self.warnings
    }
    pub fn source(&self) -> Source {
        match self.file.clone() {
            Some(path) => Source::File(path),
            None => Source::default(),
        }
    }

    pub fn human_readable(&self) -> bool {
        self.human_readable
    }

    pub fn number_of_threads(&self) -> Option<usize> {
        self.number_of_threads
    }

    pub fn sort_column(&self) -> Option<u8> {
        self.sort_column_number
    }
}
