const SI_BASE: f64 = 1000.0;
const SI_UNIT_PREFIX: [&str; 9] = ["", "K", "M", "G", "T", "P", "E", "Z", "Y"];

pub fn si_format(number: usize) -> String {
    let exp = ((number as f64).log10() / SI_BASE.log10()).floor();

    let rounded = number as f64 / (SI_BASE as f64).powf(exp);

    format!(
        "{:.1}{}",
        rounded,
        SI_UNIT_PREFIX.get(exp as usize).unwrap_or(&"?")
    )
}
