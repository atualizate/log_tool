use serde_json::{from_str as parse_json, Value as JsonValue};

use crate::errors::AppError;
use crate::utils::si_format;

#[derive(Debug)]
pub struct KeyStats {
    value: String,
    frequency: usize,
    size: usize,
}

impl KeyStats {
    pub fn new(value: String, frequency: usize, size: usize) -> Self {
        KeyStats {
            value,
            frequency,
            size,
        }
    }

    pub fn value(&self) -> &str {
        &self.value
    }
    pub fn frequency(&self) -> usize {
        self.frequency
    }
    pub fn size(&self) -> usize {
        self.size
    }
}

#[derive(Debug)]
pub struct LineInfo {
    value: String,
    size: usize,
}

impl LineInfo {
    fn new(value: String, size: usize) -> Self {
        LineInfo { value, size }
    }

    pub fn value(&self) -> &String {
        &self.value
    }
    pub fn size(&self) -> usize {
        self.size
    }

    pub fn try_from<L: AsRef<str>, K: AsRef<str>>(
        line_content: L,
        key: K,
    ) -> Result<Self, AppError> {
        parse_json(line_content.as_ref())
            .map_err(|e| e.into())
            .and_then(|json_value: JsonValue| {
                let some_value = json_value
                    .as_object()
                    .map(|json_object| json_object.get(key.as_ref()))
                    .flatten()
                    .map(|key_value| key_value.as_str())
                    .flatten();

                match some_value.map(|v| v.trim()) {
                    Some(value) => {
                        if value.is_empty() {
                            Err(AppError::KeyWithInvalidValue)
                        } else {
                            Ok(LineInfo::new(
                                value.to_string(),
                                line_content.as_ref().trim().len(),
                            ))
                        }
                    }
                    None => Err(AppError::JsonWithMissingKey),
                }
            })
    }
}

pub struct ParserResult {
    with_units: bool,
    data: Vec<KeyStats>,
}

impl ParserResult {
    pub fn new(data: Vec<KeyStats>) -> Self {
        ParserResult {
            with_units: false,
            data,
        }
    }

    pub fn with_units(mut self, with_units: bool) -> Self {
        self.with_units = with_units;

        self
    }

    pub fn sort_by(mut self, column: Option<u8>) -> Self {
        match column {
            Some(1) => self.data.sort_by(|a, b| a.value.cmp(&b.value)),
            Some(2) => self.data.sort_by(|a, b| a.frequency.cmp(&b.frequency)),
            Some(3) => self.data.sort_by(|a, b| a.size.cmp(&b.size)),
            _ => {}
        };

        self
    }
}

impl std::fmt::Display for ParserResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut output = String::new();

        let header = format!("{0: <10} {1: <10} {2: <10}\n", "type", "frequency", "size");

        output.push_str(&header);

        for entry in self.data.iter() {
            let size: String = if self.with_units {
                si_format(entry.size())
            } else {
                entry.size.to_string()
            };

            output.push_str(&format!(
                "{0: <10} {1: <10} {2: <10}\n",
                entry.value(),
                entry.frequency(),
                size
            ));
        }

        write!(f, "{}", output)
    }
}
