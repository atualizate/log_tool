use std::fmt::{Debug, Formatter};

#[derive(Clone)]
pub enum AppError {
    FileNotExists,
    FileWithoutReadPermissions(String),
    GeneralIOError(String),
    InvalidJson(usize),
    JsonWithMissingKey,
    KeyWithInvalidValue,
    ParserProcessingError(String),
}

impl Debug for AppError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

impl std::fmt::Display for AppError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            AppError::GeneralIOError(ref reason) => {
                write!(f, "IO error {}", reason)
            }
            AppError::FileNotExists => {
                write!(f, "File not found")
            }
            AppError::FileWithoutReadPermissions(ref reason) => {
                write!(f, "File permissions - {}", reason)
            }
            AppError::InvalidJson(column) => {
                write!(f, "Invalid Json object at column {}", column)
            }
            AppError::JsonWithMissingKey => {
                write!(f, "Json without key or with wrong data type")
            }
            AppError::KeyWithInvalidValue => {
                write!(f, "Key exists but not was some correct value")
            }
            AppError::ParserProcessingError(ref reason) => {
                write!(f, "Some error while parsing file: {}", reason)
            }
        }
    }
}

impl From<std::io::Error> for AppError {
    fn from(io_error: std::io::Error) -> Self {
        match io_error.kind() {
            std::io::ErrorKind::NotFound => AppError::FileNotExists,
            std::io::ErrorKind::PermissionDenied => {
                AppError::FileWithoutReadPermissions(io_error.to_string())
            }
            _ => AppError::GeneralIOError(io_error.to_string()),
        }
    }
}

impl From<serde_json::Error> for AppError {
    fn from(json_error: serde_json::Error) -> Self {
        if json_error.is_syntax() || json_error.is_data() {
            AppError::InvalidJson(json_error.column())
        } else {
            AppError::GeneralIOError(json_error.to_string())
        }
    }
}
