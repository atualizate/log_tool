use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::PathBuf;

use crossbeam::channel::{Receiver, Sender};
use rayon::prelude::*;

use crate::errors::AppError;
use crate::models::{KeyStats, LineInfo, ParserResult};

const DEFAULT_KEY: &'static str = "type";

pub enum Source {
    File(PathBuf),
    StdIn,
}

impl Default for Source {
    fn default() -> Self {
        Source::StdIn
    }
}

impl Source {
    pub fn reader(self) -> Result<Box<dyn BufRead + Send>, AppError> {
        Ok(match self {
            Source::StdIn => Box::new(BufReader::new(std::io::stdin())),
            Source::File(path) => Box::new(BufReader::new(File::open(path)?)),
        })
    }
}

pub struct ParserBuilder {
    warnings: bool,
    concurrency: Option<usize>,
    source: Source,
}

impl ParserBuilder {
    pub fn new() -> Self {
        ParserBuilder {
            warnings: false,
            concurrency: None,
            source: Source::default(),
        }
    }

    pub fn with_source(mut self, source: Source) -> Self {
        self.source = source;

        self
    }

    pub fn with_concurrency(mut self, number_of_threads: Option<usize>) -> Self {
        self.concurrency = number_of_threads;

        self
    }

    pub fn with_warnings(mut self, warnings: bool) -> Self {
        self.warnings = warnings;

        self
    }

    pub fn build(self) -> Result<impl Parser, AppError> {
        match self.concurrency {
            Some(number) if number > 0 => {
                std::env::set_var("RAYON_NUM_THREADS", number.to_string());
            }
            _ => {}
        };

        Ok(ChannelThreadedParser {
            warnings: self.warnings,
            reader: self.source.reader()?,
        })
    }
}

/// Parser interface
pub trait Parser {
    fn parse(self) -> Result<ParserResult, AppError>;
}

/// Multiple producer, single consumer parser.
struct ChannelThreadedParser<T: 'static + BufRead + Send> {
    warnings: bool,
    reader: T,
}

impl<T: 'static + BufRead + Send> Parser for ChannelThreadedParser<T> {
    /// The producers are created from the parallel ryon iterator
    /// where the task they execute is to parse the line
    /// into JSON and get the desired key value and the size
    /// of that line.
    fn parse(self) -> Result<ParserResult, AppError> {
        let show_warnings = self.warnings;

        let (tx, rx): (Sender<LineInfo>, Receiver<LineInfo>) =
            crossbeam::channel::unbounded::<LineInfo>();

        let worker = std::thread::spawn(move || {
            self.reader
                .lines()
                .enumerate()
                .par_bridge()
                .filter_map(|(line_number, line_result)| {
                    let result: Result<LineInfo, AppError> = line_result
                        .map_err(|e| e.into())
                        .and_then(|line_content| LineInfo::try_from(line_content, DEFAULT_KEY));

                    match result {
                        Ok(info) => Some(info),
                        Err(err) => {
                            if show_warnings {
                                eprintln!("Error at line {}: {}", line_number, err);
                            }
                            None
                        }
                    }
                })
                .try_for_each_with(tx, |tx, line| tx.send(line))
        });

        let mut result: HashMap<String, (usize, usize)> = HashMap::new();

        for line_info in rx {
            let (counter, bytes) = result
                .entry(line_info.value().to_string())
                .or_insert((0, 0));

            *counter += 1;
            *bytes += line_info.size();
        }

        let worker_result = worker.join();

        if let Ok(Err(err)) = worker_result {
            return Err(AppError::ParserProcessingError(err.to_string()));
        } else if let Err(_) = worker_result {
            return Err(AppError::ParserProcessingError(format!(
                "Unexpected worker thread error"
            )));
        }

        let stats = result
            .into_iter()
            .map(|(value, (counter, bytes))| KeyStats::new(value.to_string(), counter, bytes))
            .collect();

        Ok(ParserResult::new(stats))
    }
}
