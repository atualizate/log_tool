use structopt::StructOpt;

use cli::CliOptions;

use crate::parser::{Parser, ParserBuilder};

mod cli;
mod errors;
mod models;
mod parser;
mod utils;

fn main() -> Result<(), errors::AppError> {
    let options: CliOptions = CliOptions::from_args();

    let parser = ParserBuilder::new()
        .with_warnings(options.warnings())
        .with_concurrency(options.number_of_threads())
        .with_source(options.source())
        .build()?;

    let result = parser.parse()?;

    println!(
        "{}",
        result
            .with_units(options.human_readable())
            .sort_by(options.sort_column())
    );

    Ok(())
}
