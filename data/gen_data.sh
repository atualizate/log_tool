#!/bin/bash

set -e

if [ "$#" -ne 1 ]; then
  echo "USAGE: command <generate-kb>"
  exit 1
fi

ITERATIONS=$((($1 * 1000) / 27))

for ((c = 1; c <= ${ITERATIONS}; c++)); do
  number=$(((RANDOM % 26) + 65))

  KEY=$(printf "\x$(printf %x ${number})")

  echo '{"type": "'$KEY'","bar": "abcd"}'
done
